import logo from "./logo.svg";
import "./App.css";
import TaiXiuPages from "./Pages/TaiXiuPages";

function App() {
  return (
    <div className="App">
      <TaiXiuPages />
    </div>
  );
}

export default App;
