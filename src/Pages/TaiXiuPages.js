import React, { useState } from "react";
import bg_game from "../asset/bgGame.png";
import "./xucXac.css";
import XucXac from "./XucXac";
import KetQua from "./KetQua";
export const TAI = "TÀI";
export const XIU = "XỈU";
export const WIN = "YOU WIN";
export const LOSE = "YOU LOSE";

export default function TaiXiuPages() {
  const [xucXacArr, setXucXacArr] = useState([
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
  ]);

  const [luaChon, setLuaChon] = useState(null);
  let handleLuaChon = (value) => {
    setLuaChon(value);
  };

  const [soBanThang, setSoBanThang] = useState(0);
  const [soLuotChoi, setSoLuotChoi] = useState(0);
  const [result, setResult] = useState("");

  let handlePlayGame = () => {
    let tongDiem = 0;
    let newXucXacArr = xucXacArr.map((item) => {
      let number = Math.floor(Math.random() * 6) + 1;
      tongDiem += number;
      return {
        img: `./imgXucSac/${number}.png`,
        giaTri: number,
      };
    });
    setXucXacArr(newXucXacArr);

    switch (luaChon) {
      case TAI: {
        if (tongDiem >= 11) {
          setResult(WIN);
        } else {
          setResult(LOSE);
        }
        break;
      }
      case XIU: {
        if (tongDiem < 11) {
          setResult(WIN);
        } else {
          setResult(LOSE);
        }
        break;
      }
      default:
        break;
    }

    luaChon == TAI && tongDiem >= 11 && setSoBanThang(soBanThang + 1);
    luaChon == XIU && tongDiem < 11 && setSoBanThang(soBanThang + 1);

    setSoLuotChoi(soLuotChoi + 1);

    // if (luaChon == TAI && tongDiem >= 11) {
    //   setResult(WIN);
    // } else if (luaChon == TAI && tongDiem < 11) {
    //   setResult(LOSE);
    // } else if (luaChon == XIU && tongDiem < 11) {
    //   setResult(WIN);
    // } else if (luaChon == XIU && tongDiem >= 11) {
    //   setResult(LOSE);
    // }
  };

  return (
    <div className="game_container">
      <XucXac xucXacArr={xucXacArr} handleLuaChon={handleLuaChon} />
      <KetQua
        luaChon={luaChon}
        handlePlayGame={handlePlayGame}
        soBanThang={soBanThang}
        soLuotChoi={soLuotChoi}
        result={result}
      />
    </div>
  );
}
