import React from "react";
import { TAI, XIU } from "./TaiXiuPages";

export default function XucXac({ xucXacArr, handleLuaChon }) {
  let renderXucXacArr = () => {
    return xucXacArr.map((item, index) => {
      return (
        <img
          style={{ height: 100, margin: 10 }}
          src={item.img}
          key={index}
          alt=""
        />
      );
    });
  };
  return (
    <div className="p-5 d-flex justify-content-between container">
      <button
        onClick={() => {
          handleLuaChon(TAI);
        }}
        className="btn btn-danger"
      >
        Tài
      </button>
      {/* {renderXucXacArr()} */}
      <div>
        {xucXacArr?.map((item) => {
          return (
            <img style={{ height: 100, margin: 10 }} src={item.img} alt="" />
          );
        })}
      </div>
      <button
        onClick={() => {
          handleLuaChon(XIU);
        }}
        className="btn btn-danger"
      >
        Xỉu
      </button>
    </div>
  );
}
