import React from "react";

export default function KetQua({
  luaChon,
  handlePlayGame,
  soBanThang,
  soLuotChoi,
  result,
}) {
  return (
    <div>
      <button onClick={handlePlayGame} className="btn btn-warning px-5">
        Play Game
      </button>
      <h2 className="text-danger pt-3"> {result} </h2>
      <h2 className="pt-5">Bạn chọn : {luaChon} </h2>
      <h2 className="p-3">Số bàn thắng : {soBanThang} </h2>
      <h2>Số lượt chơi: {soLuotChoi} </h2>
    </div>
  );
}
